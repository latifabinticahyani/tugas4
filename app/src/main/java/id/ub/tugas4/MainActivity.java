package id.ub.tugas4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText et;
    Button bt, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9;
    Button bt10, bt11, bt12, bt13, bt14, bt15;

    TextView op;

    public static double nilaiSekarang=0;
    public static String operasiSekarang="";
    public static double hasilAkhir=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        op = findViewById(R.id.op);
    }

    public void init(){
        et = (EditText) findViewById(R.id.et);
        op = findViewById(R.id.op);
        bt = (Button)findViewById(R.id.bt);
        bt.setOnClickListener(this);
        bt1 = (Button) findViewById(R.id.bt1);
        bt1.setOnClickListener(this);
        bt2 = (Button) findViewById(R.id.bt2);
        bt2.setOnClickListener(this);
        bt3 = (Button) findViewById(R.id.bt3);
        bt3.setOnClickListener(this);
        bt4 = (Button) findViewById(R.id.bt4);
        bt4.setOnClickListener(this);
        bt5 = (Button) findViewById(R.id.bt5);
        bt5.setOnClickListener(this);
        bt6 = (Button) findViewById(R.id.bt6);
        bt6.setOnClickListener(this);
        bt7 = (Button) findViewById(R.id.bt7);
        bt7.setOnClickListener(this);
        bt8 = (Button) findViewById(R.id.bt8);
        bt8.setOnClickListener(this);
        bt9 = (Button) findViewById(R.id.bt9);
        bt9.setOnClickListener(this);

        bt10 = (Button)findViewById(R.id.bt10);
        bt10.setOnClickListener(this);
        bt11 = (Button)findViewById(R.id.bt11);
        bt11.setOnClickListener(this);
        bt12 = (Button)findViewById(R.id.bt12);
        bt12.setOnClickListener(this);
        bt13 = (Button)findViewById(R.id.bt13);
        bt13.setOnClickListener(this);
        bt14 = (Button)findViewById(R.id.bt14);
        bt14.setOnClickListener(this);
        bt15 = (Button)findViewById(R.id.bt15);
        bt15.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Button button = (Button) view;
        String buttonText = button.getText().toString();
        String operasiData = op.getText().toString();

        operasiData = operasiData+buttonText;

        op.setText(operasiData);
        switch (view.getId()){
            case R.id.bt9:
                et.setText(et.getText().toString().trim()+"0");
                break;
            case R.id.bt:
                et.setText(et.getText().toString().trim()+"1");
                break;
            case R.id.bt1:
                et.setText(et.getText().toString().trim()+"2");
                break;
            case R.id.bt2:
                et.setText(et.getText().toString().trim()+"3");
                break;
            case R.id.bt3:
                et.setText(et.getText().toString().trim()+"4");
                break;
            case R.id.bt4:
                et.setText(et.getText().toString().trim()+"5");
                break;
            case R.id.bt5:
                et.setText(et.getText().toString().trim()+"6");
                break;
            case R.id.bt6:
                et.setText(et.getText().toString().trim()+"7");
                break;
            case R.id.bt7:
                et.setText(et.getText().toString().trim()+"8");
                break;
            case R.id.bt8:
                et.setText(et.getText().toString().trim()+"9");
                break;

            case R.id.bt10:
                nilaiSekarang = Double.parseDouble(et.getText().toString());
                operasiSekarang="tambah";
                et.setText("");
                break;
            case R.id.bt11:
                nilaiSekarang = Double.parseDouble(et.getText().toString());
                operasiSekarang="kali";
                et.setText("");
                break;
            case R.id.bt13:
                operasiSekarang="kurang";
                nilaiSekarang = Double.parseDouble(et.getText().toString());
                et.setText("");
                break;
            case R.id.bt14:
                operasiSekarang="bagi";
                nilaiSekarang = Double.parseDouble(et.getText().toString());
                et.setText("");
                break;

            case R.id.bt15:
                et.setText("");
                break;
            case R.id.bt12:

                if(operasiSekarang.equals("tambah")){
                    hasilAkhir = nilaiSekarang + Double.parseDouble(et.getText().toString().trim());
                    et.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("kali")){
                    hasilAkhir = nilaiSekarang * Double.parseDouble(et.getText().toString().trim());
                    et.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("kurang")){
                    hasilAkhir = nilaiSekarang - Double.parseDouble(et.getText().toString().trim());
                    et.setText(String.valueOf((int)hasilAkhir));
                }
                if(operasiSekarang.equals("bagi")){
                    hasilAkhir = nilaiSekarang / Double.parseDouble(et.getText().toString().trim());
                    et.setText(String.valueOf((int)hasilAkhir));
                }

                int tmp = (int) hasilAkhir;
                if(tmp == hasilAkhir){
                    et.setText(String.valueOf((int)hasilAkhir));
                }else{
                    et.setText(String.valueOf(hasilAkhir));
                }

                Intent intent = new Intent(this,Activity2.class);
                intent.putExtra("hasil",tmp);

                intent.putExtra("operasi", operasiData);
                startActivity(intent);
                break;
        }

    }
}
